﻿using System.ComponentModel.DataAnnotations;
using EmployeeApi.ViewModel.Roles;

namespace EmployeeApi.ViewModel.Employee
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string CountryCode { get; set; }
        public List<RoleViewModel> Roles { get; set; }
    }
}
