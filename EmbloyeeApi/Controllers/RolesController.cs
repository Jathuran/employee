﻿using EmployeeApi.Services;
using EmployeeApi.Services.Roles;
using EmployeeApi.ViewModel.Roles;
using InfrastructureEmployeesDb.Models;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRolesService _rolesService;

        public RolesController(IRolesService rolesService)
        {
            _rolesService = rolesService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(string name)
        {
            await _rolesService.Create(new RoleViewModel(){Name = name});
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _rolesService.GetAll());
        }
    }
}
