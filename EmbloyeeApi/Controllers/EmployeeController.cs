﻿using EmployeeApi.Services;
using EmployeeApi.Services.Employees;
using EmployeeApi.ViewModel.Employee;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpPost(Name = "Create")]
        public async Task<IActionResult>  CreateEmployee(EmployeeViewModel employee)
        {
            
            await _employeeService.Create(employee);
            return Ok();
        }

        [HttpPut(Name = "Update")]
        public async Task<IActionResult>  UpdateEmployee(EmployeeViewModel employee)
        {
            await _employeeService.Update(employee);
            return Ok();
        }

        [HttpGet(Name = "GetAll")]
        public async Task<IActionResult> GetALl()
        {
            return Ok(await _employeeService.GetAll());
        }

        [HttpDelete(Name = "Delete")]
        public async Task<IActionResult> Delete(int id)
        {
            await _employeeService.Delete(id);
            return Ok();
        }
    }
}
