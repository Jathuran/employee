﻿using EmployeeApi.ViewModel.Roles;
using InfrastructureEmployeesDb;
using InfrastructureEmployeesDb.Models;
using Microsoft.EntityFrameworkCore;

namespace EmployeeApi.Services.Roles
{
    public class RolesService : IRolesService
    {
        private readonly Context _dbContext;

        public RolesService(Context context)
        {
            _dbContext = context;
        }

        public async Task<List<RoleViewModel>>  GetAll()
        {
            return await _dbContext.Roles.Select(role => new RoleViewModel(){Id = role.Id, Name = role.Name}).ToListAsync();
        }

        public async Task  Create(RoleViewModel role)
        {
            _dbContext.Roles.Add(new Role(){Id = role.Id, Name = role.Name});
            await _dbContext.SaveChangesAsync();
        }
    }
}
