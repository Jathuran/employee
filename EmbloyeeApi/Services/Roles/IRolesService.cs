﻿using EmployeeApi.ViewModel.Roles;

namespace EmployeeApi.Services.Roles
{
    public interface IRolesService
    {
        Task<List<RoleViewModel>>  GetAll();
        Task Create(RoleViewModel role);
    }
}
