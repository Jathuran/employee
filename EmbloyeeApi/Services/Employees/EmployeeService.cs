﻿using EmployeeApi.ViewModel.Employee;
using EmployeeApi.ViewModel.Roles;
using InfrastructureEmployeesDb;
using InfrastructureEmployeesDb.Models;
using Microsoft.EntityFrameworkCore;


namespace EmployeeApi.Services.Employees
{
    public class EmployeeService : IEmployeeService
    {
        private readonly Context _dbContext;

        public EmployeeService(Context dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(EmployeeViewModel employee)
        {
            var roles = new List<Role>();
            employee.Roles.ForEach(role => roles.Add(_dbContext.Roles.FirstOrDefault(dbRole => dbRole.Id == role.Id)));

            _dbContext.Employees.Add(ConvertViewModelToDbModel(employee, roles));
            
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var employee = _dbContext.Employees.First(employee => employee.Id == id);
            _dbContext.Employees.Remove(employee);
            await _dbContext.SaveChangesAsync();
        }
        

        public async Task Update(EmployeeViewModel employee)
        {
            var readEmployee = _dbContext.Employees.First(dbEmployee => dbEmployee.Id == employee.Id);

            var addedRoles = employee.Roles.Where(role => !readEmployee.Roles.Any(existingRole => existingRole.Id == role.Id)).ToList();
            addedRoles.ForEach(role => readEmployee.Roles.Add(new InfrastructureEmployeesDb.Models.Role() { Id = role.Id, Employees = new List<InfrastructureEmployeesDb.Models.Employee>() { readEmployee }, Name = role.Name }));

            var removedRoles = readEmployee.Roles.Where(role => !employee.Roles.Any(existingRole => existingRole.Id == role.Id)).ToList();
            removedRoles.ForEach(role => readEmployee.Roles.Remove(role));

            readEmployee.FirstName = employee.FirstName;
            readEmployee.LastName = employee.LastName;
            readEmployee.CountryCode = employee.CountryCode;
            readEmployee.DateOfBirth = employee.DateOfBirth;
            readEmployee.PhoneNumber = employee.PhoneNumber;

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<EmployeeViewModel>> GetAll()
        {
            return ConvertDbModelToViewModel(await _dbContext.Employees.ToListAsync());
        }

        private Employee ConvertViewModelToDbModel(EmployeeViewModel viewModel, List<Role> roles)
        {
            return new Employee()
            {
                Id = viewModel.Id,
                CountryCode = viewModel.CountryCode,
                DateOfBirth = viewModel.DateOfBirth,
                FirstName = viewModel.FirstName,
                LastName = viewModel.LastName,
                PhoneNumber = viewModel.PhoneNumber,
                Roles = roles
                
            };
        }

        private List<EmployeeViewModel> ConvertDbModelToViewModel(List<InfrastructureEmployeesDb.Models.Employee> employees)
        {
            var employeesViewModel= employees.Select(x => new EmployeeViewModel()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                PhoneNumber = x.PhoneNumber,
                CountryCode = x.CountryCode,
                DateOfBirth = x.DateOfBirth,
                Roles = x.Roles.Select(y => new RoleViewModel() {Id = y.Id, Name = y.Name}).ToList()
            }).ToList();

            return employeesViewModel;
        }
    }
}
