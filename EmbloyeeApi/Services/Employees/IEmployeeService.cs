﻿using EmployeeApi.ViewModel.Employee;

namespace EmployeeApi.Services.Employees
{
    public interface IEmployeeService
    {
        Task Create(EmployeeViewModel employee);
        Task Delete(int id);
        Task Update(EmployeeViewModel employee);
        Task<List<EmployeeViewModel>> GetAll();
    }
}
