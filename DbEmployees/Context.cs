﻿using InfrastructureEmployeesDb.Models;
using Microsoft.EntityFrameworkCore;

namespace InfrastructureEmployeesDb
{
    public class Context: DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }

        public Context(DbContextOptions<Context> options) : base(options)
        {
            
        }
    }
}
