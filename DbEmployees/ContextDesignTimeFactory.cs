﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace InfrastructureEmployeesDb
{
    public class ContextDesignTimeFactory : IDesignTimeDbContextFactory<Context>
    {
        public Context CreateDbContext(string[] args)
        {
            var optionBuilder = new DbContextOptionsBuilder<Context>().UseSqlServer(@"Server=LAPTOP-HOL8E8O1\SQLEXPRESS;Database=Customer;Trusted_Connection=True;");
            return new Context(optionBuilder.Options);
        }
    }
}
