﻿using System.ComponentModel.DataAnnotations.Schema;

namespace InfrastructureEmployeesDb.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; } 
        public string PhoneNumber { get; set; }
        public string CountryCode { get; set; }
        public virtual List<Role> Roles { get; set; }
    }
}